from django import forms
from .models import Word, Comment, Url, ApiMeme
from django.contrib.auth.models import User


class WordSearchForm(forms.ModelForm):
    class Meta:
        model = Word
        fields = {'name'}

    name = forms.CharField(label='', required=False)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = {'text'}

    text = forms.CharField(widget=forms.Textarea(attrs={"rows":9, "cols":15}), label='', required=False)


class UrlForm(forms.ModelForm):
    class Meta:
        model = Url
        fields = {'link'}

    link = forms.CharField(widget=forms.Textarea(attrs={"rows":9, "cols":15}), label='', required=False)


class MemeForm(forms.ModelForm):
    MEME_CHOICE = [
        ('Angry-Baby', 'Angry Baby'),
        ('Baby-Godfather', 'Baby Godfather'),
        ('Beard-Baby', 'Beard Baby'),
        ('Angry-Toddler', 'Angry Toddler')
    ]

    class Meta:
        model = ApiMeme
        fields = {'text', 'image'}

    text = forms.CharField(widget=forms.Textarea(attrs={"rows":9, "cols":15}), label='Introduzca un texto:', required=False)
    image = forms.CharField(label='Seleccione una imagen:', widget=forms.Select(choices=MEME_CHOICE), required=False)


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())
