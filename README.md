# Final MisPalabras

Práctica final curso 2021-2022 (MisPalabras)

# Entrega práctica

## Datos
* Nombre: Estela Cameo Pereira
* Titulación: Ingeniería en Telemática
* Despliegue (url): http://estelacp.pythonanywhere.com
* Video básico (url): https://youtu.be/lCEozjouvlg
* Video parte opcional (url): https://youtu.be/UECyZQBKPEs
* Usuario Gitlab: estelacp
* Usuario Laboratorio: estelacp

## Cuenta Admin Site
* admin0/admin0

## Cuentas usuarios
* admin/admin
* estela/estela
* estelacp/1234
* pepe/pepe

## Resumen parte obligatoria
Contiene lo que indica el enunciado respecto a los distintos apartados de la parte obligatoria. Podemos observar dicho contenido en el video básico (url) adjuntado arriba.

## Lista partes opcionales
* Búsqueda de palabras compuestas, nombres propios y nombres con tilde.
* Posibilidad de quitar un voto.
* Inclusión de un favicon.
* Posibilidad de eliminar una cuenta, así como sus aportaciones.

## Resumen de peculiaridades
Si lanzamos nuestro proyecto en pythonanywhere, no se muestra la imagen en el fondo de la cabecera, sólo aparece un fondo blanco. Además, no podemos añadir la definición del DRAE, ni Flickr, ni una url de una palabra.

